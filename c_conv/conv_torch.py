import numpy as np
import torch
from torch.autograd import Function
from typing import Any

from conv import conv, conv_bw


class Conv(Function):

    """
    PyTorch Function wrapper for a C implementation of a 2D convolution (and its gradient)
    """

    n_threads = None

    @staticmethod
    def forward(ctx: Any, filter: torch.Tensor, image: torch.Tensor, stride=1) -> torch.Tensor:
        """

        :param ctx: PyTorch context to carry information to the backward pass
        :param filter: 4D filter tensor [n_filters] x [n_in_channels] x [f_height] x [f_width]
        :param image: 4D image (or feature map) tensor [batch_size] x [n_in_channels] x [im_height] x [im_width]
        :param stride: int represening the filter stride along both dimensions
        :return: 4D convolution tensor [batch_size] x [n_filters] x (([im_height]-[f_height])//stride+1) x
                                                                  x (([im_width]-[f_width])//stride+1)
        """
        # use the C extension to calculate the convolution
        # (note that the numpy arrays share the same memory as the tensors (i.e. we avoid copying),
        # unless one is not C contiguous, which normally shouldn't happen)
        result_np = conv(np.ascontiguousarray(filter.numpy()), np.ascontiguousarray(image.numpy()), stride,
                         n_threads=Conv.n_threads)

        # the following also doesn't copy the data
        result = torch.as_tensor(result_np)
        # result.requires_grad_()

        # save values for backward pass
        ctx.save_for_backward(filter, image)
        ctx.stride = stride

        return result

    @staticmethod
    def backward(ctx: Any, d_res_bfhw: torch.Tensor) -> (torch.Tensor, torch.Tensor, None):
        """
        Gradients of the 2D convolution w.r.t. the image and filter respectively.
        :param ctx: context carried over from the forward pass
        :param d_res_bfhw: 4D gradient tensor of the loss with respect to the output
        :return: d_filter, d_image, None - 2 4D tensors carrying the gradients w.r.t. filter and image respectively
                                           (None is to match the stride input)
        """
        filter, image = ctx.saved_tensors
        d_filter_fchw, d_image_bchw = conv_bw(np.ascontiguousarray(d_res_bfhw.numpy()), filter.numpy(), image.numpy(),
                                              ctx.stride)
        return torch.as_tensor(d_filter_fchw), torch.as_tensor(d_image_bchw), None


cconv = Conv.apply
