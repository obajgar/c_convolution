#ifndef CONV_PARAL_H
#define CONV_PARAL_H

extern void pconv(int batch_size, int in_channels, int n_filters, int filter_rows, int filter_cols,
           float *filter,
           int im_rows, int im_cols,
           float *image,
           int stride,
           float *result,
           int n_threads);

#endif