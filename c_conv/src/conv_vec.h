#ifndef CONV_VEC_H
#define CONV_VEC_H

extern void print_mat(int rows, int cols, float mat[]);

extern float dot(int rows, int cols, float *filter, float *window);

extern void conv_simple(int in_channels, int filter_rows, int filter_cols,
           float *filter,
           int im_rows, int im_cols,
           float *image,
           int stride,
           float *result);

extern void conv(int batch_size, int in_channels, int n_filters, int filter_rows, int filter_cols,
           float *filter,
           int im_rows, int im_cols,
           float *image,
           int stride,
           float *result);

extern void bw_conv(int batch_size, int in_channels, int n_filters, int filter_rows, int filter_cols,
           float *filter,   // filter used for the forward pass
           float *d_filter, // array to store the derivative with respect to the filter
           int im_rows, int im_cols,
           float *image,  // image used for the forward pass
           float *d_image,  // array to store the gradient w.r.t. the image input
           int stride,
           float *d_result  // gradient of the output coming from later layers
           );

#endif