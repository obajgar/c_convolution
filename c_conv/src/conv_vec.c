#include <stdio.h>

#include "conv_vec.h"

void print_mat(int rows, int cols, float mat[]){
  // Prints the matrix mat with the provided number of rows and columns
  // (included mainly for testing purposes)
  for(int i=0; i<rows; i++){
    for(int j=0; j<cols; j++){
      printf("%3.2f \t", mat[i*cols+j]);
    }
    puts("\n");
  }
}

float dot(int rows, int cols, float *filter, float *window){
  // Calculates the matrix dot product of filter and window
  // (included mainly for testing purposes)
  float product = 0;
  for(int i=0; i<rows; i++){
    for(int j=0; j<cols; j++){
      product += filter[i*cols+j] * window[i*cols+j];
    }
  }
  return product;
}

void conv_simple(int in_channels, int filter_rows, int filter_cols,
           float *filter,
           int im_rows, int im_cols,
           float *image,
           int stride,
           float *result){
    // Calculates the 2D convolution of a filter with an image
    // (single filter, single image)

    float product;
    int result_cols = (im_cols-filter_cols)/stride + 1;

    // Filter channel multiplier (to get the flattened array index)
    int fcm = filter_rows * filter_cols;
    int icm = im_rows * im_cols;

    // Position of the filter in the image
    int x;
    int y;

    // Iterate over window positions k,l
    for(int k=0; k * stride + filter_rows <= im_rows; k++){
      for(int l=0; l * stride + filter_cols <= im_cols; l++){
        x = k*stride;
        y = l*stride;
        product = 0.;
        // Iterate over input channels
        for(int c=0; c<in_channels; c++){
          // Iterate over filter positions
          for(int i=0; i<filter_rows; i++){
            for(int j=0; j<filter_cols; j++){
              product += filter[c*fcm + i*filter_cols+j] * image[c*icm+(x+i)*im_cols + y+j];
            }
          }
        }
        result[k*result_cols + l] = product;
      }
    }
}


void conv(int batch_size, int in_channels, int n_filters, int filter_rows, int filter_cols,
           float *filter,
           int im_rows, int im_cols,
           float *image,
           int stride,
           float *result){
    // Calculates the 2D convolution of a set of filters with a batch of (multi-channel) images

    // Result dimensions
    int result_rows = (im_rows-filter_rows)/stride + 1;
    int result_cols = (im_cols-filter_cols)/stride + 1;

    // Input- and result- batch dimension multipliers - what we need to multiply the batch number by
    // to get the correct 1-D array index
    int ibm = in_channels * im_rows * im_cols;
    int rbm = n_filters * result_rows * result_cols;

    // Same for the channel indices in image and filter respectively
    // and the filter indices in the filter and result arrays respectively
    int ffm = in_channels * filter_rows * filter_cols;
    int rfm = result_rows * result_cols;


    // Iterate over batches
    for (int b=0; b<batch_size; b++){
      // Iterate over filters
      for(int f=0; f<n_filters; f++){
        // Over input channels
        conv_simple(in_channels, filter_rows, filter_cols, filter + f*ffm,
                    im_rows, im_cols, image + b*ibm,
                    stride,
                    result + b*rbm + f*rfm);
      }
    }
}

void bw_conv_simple(int filter_rows, int filter_cols,
           float *filter,   // filter used for the forward pass
           float *d_filter, // array to store the derivative with respect to the filter
           int im_rows, int im_cols,
           float *image,  // image used for the forward pass
           float *d_image,  // array to store the gradient w.r.t. the image input
           int stride,
           float *d_result  // gradient of the output coming from later layers
           ){
    // Calculates the gradient of a 2D convolution
    // (single filter, single image, single input channel)

    int result_cols = (im_cols-filter_cols)/stride +1;
    // Iterate over filter positions x,y (upper left corner of the filter)
    for(int k=0; k * stride + filter_rows <= im_rows; k++){
      for(int l=0; l * stride + filter_cols <= im_cols; l++){
        for(int i=0; i<filter_rows; i++){
          for(int j=0; j<filter_cols; j++){
            d_image[(k*stride + i)*im_cols+l*stride+j] += d_result[k*result_cols + l] * filter[i*filter_cols+j];
            d_filter[i*filter_cols+j] += d_result[k*result_cols + l] * image[(k*stride + i)*im_cols+l*stride+j];
          }
        }
      }
    }
}


void bw_conv(int batch_size, int in_channels, int n_filters, int filter_rows, int filter_cols,
           float *filter,   // filter used for the forward pass
           float *d_filter, // array to store the derivative with respect to the filter
           int im_rows, int im_cols,
           float *image,  // image used for the forward pass
           float *d_image,  // array to store the gradient w.r.t. the image input
           int stride,
           float *d_result  // gradient of the output coming from later layers
           ){
    // Calculates the gradient of a 2D convolution of a set of filters with a batch of (multi-channel) images

    // Result dimensions
    int result_rows = (im_rows-filter_rows)/stride +1;
    int result_cols = (im_cols-filter_cols)/stride +1;

    // Input- and result- batch dimension multipliers - what we need to multiply the batch number by
    // to get the correct 1-D array index
    int ibm = in_channels * im_rows * im_cols;
    int rbm = n_filters * result_rows * result_cols;

    // Same for the channel indices in image and filter respectively
    // and the filter indices in the filter and result arrays respectively
    int icm = im_rows * im_cols;
    int fcm = filter_rows * filter_cols;
    int ffm = in_channels * fcm;
    int rfm = result_rows * result_cols;

    // Iterate over batches
    for (int b=0; b<batch_size; b++){
      // Iterate over filters
      for(int f=0; f<n_filters; f++){
        // Over input channels
        for(int c=0; c<in_channels; c++){
          bw_conv_simple(filter_rows, filter_cols,
                          filter + f*ffm + c*fcm, d_filter + f*ffm + c*fcm,
                          im_rows, im_cols, image + b*ibm + c*icm, d_image + b*ibm + c*icm,
                          stride,
                          d_result + b*rbm + f* rfm);
        }
      }
    }
}