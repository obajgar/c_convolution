#include <stdio.h>
#include "conv_vec.h"

int main(void){
  float f[2][2] = {
    {2.,0.},
    {0.,1.}
  };

  float w[2][2] = {
    {10.,20.},
    {50.,100.}
  };

  float prod = dot(2,2,f,w);

  puts("f=\n");
  print_mat(2,2,f);

  puts("w=\n");
  print_mat(2,2,w);

  printf("Dot product f . w = %f\n", prod);

  float im[6][4] = {
    {1.,0.,1.,0.},
    {1.,0.,1.,0.},
    {1.,0.,1.,0.},
    {2.,0.,3.,0.},
    {1.,0.,1.,0.},
    {1.,0.,1.,0.},
  };

  float conv_res[5][3];

  puts("image=\n");
  print_mat(6,4,im);

  conv(2,2,f,6,4,im,1,conv_res);

  puts("Convolution result:\n");
  print_mat(5,3,conv_res);

}
