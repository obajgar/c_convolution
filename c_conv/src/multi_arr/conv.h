extern void print_mat(int rows, int cols, float[rows][cols]);
extern float dot(int rows, int cols, float filter[rows][cols], float window[rows][cols]);
extern void conv(int filt_rows, int filt_cols, float[filt_rows][filt_cols],
                 int im_rows, int im_cols, float[im_rows][im_cols],
                 int stride,
                 float[(im_rows-filt_rows+1)/stride][(im_cols-filt_cols+1)/stride]);
