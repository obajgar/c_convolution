#include <stdlib.h>
#include <math.h>
#include <stdio.h>

#include "conv.h"

void print_mat(int rows, int cols, float mat[rows][cols]){
  // Prints the matrix mat with the provided number of rows and columns
  for(int i=0; i<rows; i++){
    for(int j=0; j<cols; j++){
      printf("%3.2f \t", mat[i][j]);
    }
    puts("\n");
  }
}


float dot(int rows, int cols, float filter[rows][cols], float window[rows][cols]){
  // Calculates the matrix dot product of filter and window
  float product = 0;
  for(int i=0; i<rows; i++){
    for(int j=0; j<cols; j++){
      product += filter[i][j] * window[i][j];
    }
  }
  return product;
}

void conv(int filter_rows, int filter_cols,
           float filter[filter_rows][filter_cols],
           int im_rows, int im_cols,
           float image[im_rows][im_cols],
           int stride,
           float result[(im_rows-filter_rows+1)/stride][(im_cols-filter_cols+1)/stride]){
    // Calculates the 2D convolution of a filter with an image
    float product;
    // Iterate over filter positions x,y (upper left corner of the filter)
    for(int k=0; k * stride + filter_rows <= im_rows; k++){
      for(int l=0; l * stride + filter_cols <= im_cols; l++){
        product = 0;
        for(int i=0; i<filter_rows; i++){
          for(int j=0; j<filter_cols; j++){
            product += filter[i][j] * image[k*stride + i][l*stride+j];
          }
        }
        result[k][l] = product;
      }
    }
}
