import numpy as np

from c_conv.src cimport cconv
from c_conv.src cimport cconv_p


def print_mat(mat: np.array):
  """
  Prints a 2D numpy array using a C function
  :param mat: 
  :return: 
  """
  cdef float[:, ::1] mat_memview = mat

  return cconv.print_mat(mat_memview.shape[0], mat_memview.shape[1], &mat_memview[0,0])


def dot(filt: np.array, window: np.array):
  """
  Matrix dot product
  :param mat:
  :return:
  """
  assert filt.shape == window.shape

  cdef float[:, ::1] filt_memview = filt
  cdef float[:, ::1] win_memview = window

  return cconv.dot(win_memview.shape[0], win_memview.shape[1], &filt_memview[0,0], &win_memview[0,0])


def conv_simple(filt: np.array, image: np.array, stride=1):
  """
  Returns a convolution of a single filter with a single-channel image
  :param mat:
  :return:
  """

  output_dims = (int((image.shape[0]-filt.shape[0])/stride)+1, int((image.shape[1]-filt.shape[1])/stride)+1)
  res = np.zeros(output_dims, dtype='float32')

  cdef float[:, ::1] filt_memview = filt
  cdef float[:, ::1] im_memview = image
  cdef float[:, ::1] res_memview = res


  cconv.conv_simple(1, filt_memview.shape[0], filt_memview.shape[1], &filt_memview[0,0],
                   im_memview.shape[0], im_memview.shape[1], &im_memview[0,0],
                   stride, &res_memview[0,0])

  return res


def conv(filt: np.array, image: np.array, stride=1, n_threads=None):
  """
  Convolution
  :param filt: np.array - a 2-D to 4-D filter [n_filters]x[n_channels]x[filter_height]x[filter_width]
  :param image: np.array - a 2-D to 4-D array representing (a batch of) image(s) [batch_size]x[n_channels]x[height]x[width]
  :return: np.array - a 4-D array representing the convolution feature map [batch_size]x[n_filters]x[height]x[width]
  """

  # If necessary, add the n_filters and n_channels dimensions to the filter
  while filt.ndim < 4:
    filt = np.expand_dims(filt, 0)

  # If necessary, add the batch and channel dimensions to the image
  while image.ndim < 4:
    image = np.expand_dims(image, 0)

  assert filt.shape[1] == image.shape[1], "Number of filter-channels must match the number of image channels."

  output_dims = (image.shape[0], filt.shape[0],
                 (image.shape[2]-filt.shape[2])//stride+1, (image.shape[3]-filt.shape[3])//stride+1)

  res = np.zeros(output_dims, dtype='float32')

  # Create memory views to be passed as pointers to the C-function
  cdef float[:, :, :, ::1] filt_memview = filt
  cdef float[:, :, :, ::1] im_memview = image
  cdef float[:, :, :, ::1] res_memview = res

  if n_threads is None:
    cconv.conv(im_memview.shape[0], im_memview.shape[1], # Batch size and in_channels
                filt_memview.shape[0], filt_memview.shape[2], filt_memview.shape[3], &filt_memview[0,0,0,0],
                im_memview.shape[2], im_memview.shape[3], &im_memview[0,0,0,0],
                stride, &res_memview[0,0,0,0])
  else:
    cconv_p.pconv(im_memview.shape[0], im_memview.shape[1], # Batch size and in_channels
                filt_memview.shape[0], filt_memview.shape[2], filt_memview.shape[3], &filt_memview[0,0,0,0],
                im_memview.shape[2], im_memview.shape[3], &im_memview[0,0,0,0],
                stride, &res_memview[0,0,0,0], n_threads)

  return res

def conv_bw(d_out: np.array, filt: np.array, image: np.array, stride: int):
  """
  Convolution
  :param d_out: gradient of the output
  :param filt: np.array - a 2-D to 4-D filter [n_filters]x[n_channels]x[filter_height]x[filter_width]
  :param image: np.array - a 2-D to 4-D array representing (a batch of) image(s) [batch_size]x[n_channels]x[height]x[width]
  :return: np.array - a 4-D array representing the convolution feature map [batch_size]x[n_filters]x[height]x[width]
  """

  # If necessary, add the n_filters and n_channels dimensions to the filter
  while filt.ndim < 4:
    filt = np.expand_dims(filt, 0)

  # If necessary, add the batch and channel dimensions to the image
  while image.ndim < 4:
    image = np.expand_dims(image, 0)

  assert filt.shape[1] == image.shape[1], "Number of filter-channels must match the number of image channels."

  output_dims = (image.shape[0], filt.shape[0],
                 int((image.shape[2]-filt.shape[2])/stride)+1, int((image.shape[3]-filt.shape[3])/stride)+1)

  assert d_out.shape == output_dims, "Output gradient's shape doesn't match the shapes of the filter and image"

  d_filter = np.zeros_like(filt)
  d_image = np.zeros_like(image)

  # Create memory views to be passed as pointers to the C-function
  cdef float[:, :, :, ::1] filt_memview = filt
  cdef float[:, :, :, ::1] im_memview = image
  cdef float[:, :, :, ::1] d_out_memview = d_out

  cdef float[:, :, :, ::1] d_filter_memview = d_filter
  cdef float[:, :, :, ::1] d_image_memview = d_image

  cconv.bw_conv(im_memview.shape[0], im_memview.shape[1], # Batch size and in_channels
                 filt_memview.shape[0], filt_memview.shape[2], filt_memview.shape[3],
                 &filt_memview[0,0,0,0], &d_filter_memview[0,0,0,0],
                 im_memview.shape[2], im_memview.shape[3],
                 &im_memview[0,0,0,0], &d_image_memview[0,0,0,0],
                 stride, &d_out_memview[0,0,0,0])

  return d_filter, d_image
