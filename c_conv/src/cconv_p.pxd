# distutils: sources = src/conv_paral.c
# distutils: include_dirs = src/


cdef extern from "conv_paral.h":

  extern void pconv(int batch_size, int in_channels, int n_filters,
                      int filter_rows, int filter_cols,
                      float *filt,
                      int im_rows, int im_cols,
                      float *image,
                      int stride,
                      float *result,
                      int n_threads)

  extern void bw_pconv(int batch_size, int in_channels, int n_filters,
                      int filter_rows, int filter_cols,
                      float *filt, float *d_filt,
                      int im_rows, int im_cols,
                      float *image, float *d_image,
                      int stride,
                      float *result,
                      int *n_threads)