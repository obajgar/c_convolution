# distutils: sources = src/conv_vec.c
# distutils: include_dirs = src/


cdef extern from "conv_vec.h":
  extern void print_mat(int rows, int cols, float *)

  extern float dot(int, int, float *, float *)

  extern void conv_simple(int in_channels, int filter_rows, int filter_cols,
                      float *filt,
                      int im_rows, int im_cols,
                      float *image,
                      int stride,
                      float *result)

  extern void conv(int batch_size, int in_channels, int n_filters,
                      int filter_rows, int filter_cols,
                      float *filt,
                      int im_rows, int im_cols,
                      float *image,
                      int stride,
                      float *result)

  extern void bw_conv(int batch_size, int in_channels, int n_filters,
                      int filter_rows, int filter_cols,
                      float *filt, float *d_filt,
                      int im_rows, int im_cols,
                      float *image, float *d_image,
                      int stride,
                      float *result)