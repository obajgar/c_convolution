#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

#include "conv_paral.h"


// The following struct will be used to pass arguments to the inner convolution function _conv_simple
// since pthread_create can pass only one argument
struct conv_args{
    int n_filters, in_channels, filter_rows, filter_cols, im_rows, im_cols, stride;
    float *filter, *image, *result, *d_filter, *d_image;
};

typedef struct conv_args cargs;


void * _conv_simple(cargs *args){
    // Calculates the 2D convolution of a filter with an image
    // (single filter, single image)

    float product;
    int result_cols = (args->im_cols - args->filter_cols) / args->stride + 1;
    int result_rows = (args->im_rows - args->filter_rows) / args->stride + 1;

    // Filter channel multiplier (to get the flattened array index)
    int fcm = args->filter_rows * args->filter_cols;
    int icm = args->im_rows * args->im_cols;

    // Filter offset for filter and result array respectively
    int f_off_f;
    int f_off_res;

    // Position of the filter in the image
    int x;
    int y;

    // Iterate over filters
    for(int f=0; f<args->n_filters; f++){
        f_off_f = f * args->in_channels * args->filter_rows * args->filter_cols;
        f_off_res = f * result_rows * result_cols;

        // Iterate over window positions k,l
        for(int k=0; k * args->stride + args->filter_rows <= args->im_rows; k++){
          for(int l=0; l * args->stride + args->filter_cols <= args->im_cols; l++){
            x = k*args->stride;
            y = l*args->stride;
            product = 0.;
            // Iterate over input channels
            for(int c=0; c<args->in_channels; c++){
              // Iterate over filter positions
              for(int i=0; i<args->filter_rows; i++){
                for(int j=0; j<args->filter_cols; j++){
                  product += args->filter[f_off_f + c*fcm + i*args->filter_cols+j] * args->image[c*icm+(x+i)*args->im_cols + y+j];
                }
              }
            }
            args->result[f_off_res + k*result_cols + l] = product;
          }
        }
    }
}


void pconv(int batch_size, int in_channels, int n_filters, int filter_rows, int filter_cols,
           float *filter,
           int im_rows, int im_cols,
           float *image,
           int stride,
           float *result,
           int n_threads){
    // Calculates the 2D convolution of a set of filters with a batch of (multi-channel) images
    // making use of multiple threads

    // Result dimensions
    int result_rows = (im_rows-filter_rows)/stride + 1;
    int result_cols = (im_cols-filter_cols)/stride + 1;

    // Input- and result- batch dimension multipliers - what we need to multiply the batch number by
    // to get the correct 1-D array index
    int ibm = in_channels * im_rows * im_cols;
    int rbm = n_filters * result_rows * result_cols;

    // Create execution threads
    pthread_t tid[n_threads];
    cargs *convargs[n_threads];
    for(int i=0; i<n_threads; i++){
        convargs[i] = (cargs *) malloc(sizeof(cargs));
        // Populate constant arguments
        convargs[i]->n_filters = n_filters;
        convargs[i]->in_channels = in_channels;
        convargs[i]->filter_rows = filter_rows;
        convargs[i]->filter_cols = filter_cols;
        convargs[i]->im_rows = im_rows;
        convargs[i]->im_cols = im_cols;
        convargs[i]->stride = stride;
        convargs[i]->filter = filter;
    }

    // Iterate over batches
    int b=0;
    int t=0;
    while(b<batch_size){
      for(t=0; t<n_threads && b<batch_size; t++){
        convargs[t]->image = image + b*ibm;
        convargs[t]->result = result + b*rbm;
        pthread_create(&tid[t], NULL, _conv_simple, convargs[t]);

        b++;
      }
      // Join threads
      for(int s=0; s<t; s++){
        pthread_join(tid[s], NULL);
      }
    }
}
