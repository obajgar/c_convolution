import torch as th
import torch.nn.functional as F
import torch.nn.grad as G
from c_conv.conv_torch import Conv

"""
Tests the conv_torch.Conv function including the gradient computation
"""

stride = 2

a = th.tensor([[1., 2.], [3., 4.]], requires_grad=True)
b = th.tensor([[1., 1.], [0, 0]], requires_grad=True)

image = th.eye(4, dtype=th.float32, requires_grad=True)
image_np = image.detach().numpy()


print("_____________")
print("AUTOGRAD TEST")

tconv = Conv.apply

print()
print("Test with batch_size=2 (2 images) and n_filters=2 :")
print("conv([a,2*a],[image, 3*image], stride=2)=")


a2a_fchw = th.unsqueeze(th.stack([a,2*a]),1)
image_bchw = th.unsqueeze(th.stack([image,3*image]),1)

a2a_fchw.retain_grad()
image_bchw.retain_grad()

res = tconv(
    a2a_fchw,
    image_bchw,
    stride)
print(res)
res.retain_grad()
print()
loss = th.sum(res)

loss.backward()

print("grad_a2a_fchw=")
print(a2a_fchw.grad)

print("grad_im=")
print(image_bchw.grad)

res_grad = res.grad

print("Check with inbuilt Pytorch functions:")
res_th = F.conv2d(image_bchw, a2a_fchw, stride=stride)
#print("conv2d([a,2*a],[image, 3*image], stride=2)=")
#print(res_th)

#print("grad_a2a_fchw=")
a2a_grad_torch = G.conv2d_weight(image_bchw, a2a_fchw.shape, res_grad, stride=stride)
#print(a2a_grad_torch)

#print("grad_im=")
im_grad_torch = G.conv2d_input(image_bchw.shape, a2a_fchw, res_grad, stride=stride)
#print(im_grad_torch)
#print()
print(f"Test 0: res_th == res: \t {th.all(res_th == res)}")

print(f"Test 1: a2a_grad_torch == a2a_fchw.grad: \t {th.all(a2a_grad_torch == a2a_fchw.grad)}")
print(f"Test 2: image_bchw.grad == im_grad_torch: \t {th.all(image_bchw.grad == im_grad_torch)}")

print()
print("Tests with large random vectors")
filter = th.randn((10,3,28,52), requires_grad=True)
image = th.randn((32,3,28,52), requires_grad=False)

res_c = tconv(filter, image, stride)
res_th = F.conv2d(image,filter,stride=stride)
err = th.sqrt(th.mean(th.square(res_th - res_c)))

tol = 1e-3

print(f"Test 0: res_th == res_c: \t {err<tol}")
print(f"Error: {err}")
