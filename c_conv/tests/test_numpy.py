import numpy as np
import conv

a = np.array([[1.,2.],[3.,4.]], dtype='float32')
b = np.array([[1.,1.],[0,0]], dtype='float32')


print("a=")
conv.print_mat(a)

print(f"||a||^2 = {conv.dot(a,a)}")

print(f"a . b = {conv.dot(a,b)}")

image = np.eye(4,4, dtype='float32')

print("image=")
conv.print_mat(image)

print("conv_simple(a,image,stride=1)=")
print(conv.conv_simple(a,image,1))

print("conv(a,image,stride=1)=")
print(conv.conv(a,image,1))

print("conv(a,image,stride=2)=")
print(conv.conv(a,image,2))

print()
print("Test with batch_size=2 (2 images) and n_filters=2 :")
print("conv([a,2*a],[image, 3*image], stride=2)=")
res = conv.conv(
    np.expand_dims([a,2*a],1),
    np.expand_dims([image,3*image],1),
    2)
print(res)

print()
print("Test a backward pass batch_size=2 (2 images) and n_filters=2 with a random normal result gradient:")
print("conv_bw(randn, [a,2*a],[image, 3*image], stride=2)=")
d_res = np.random.normal(0,1,res.shape).astype('float32')
print(conv.conv_bw(
    d_res,
    np.expand_dims([a,2*a],1),
    np.expand_dims([image,3*image],1),
    2))
