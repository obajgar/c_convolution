# c_conv

The c_conv package implements 2D convolution and its gradient in C and makes them accessible from Python. 
It can thus be used with Numpy arrays or Torch Tensors. It also contains a torch.autograd.Function
that supports automated differentiation through the convolution. 

## Installation

`pip install git+https://gitlab.com/obajgar/c_convolution`

## Code
- `src/conv_vec.c` contains the basic implementation of 2D convolution and its gradients in C
- `src/conv_paral.c` contains a multi-thread implementation of the 2D convolution only
- `src/cconv.pxd` and `src/cconv_p.pxd` contain the Cython declaration of the above libraries respectively
- `src/conv.pyx` wraps the above into Python-compatible functions that can handle Numpy arrays
- `src/conv.c` contains all of the above translated into C using Cython
- `conv_torch.py` wraps the convolution into an operation compatible with Pytorch autograd
- `test/` contains a Numpy and Pytorch test

## Usage

`c_conv/tests/` contains tests of the library with numpy arrays as well as pytorch tensors.  However, the usage is also explained here:

### From PyTorch
`c_conv.conv_torch.cconv` can be used as any torch function e.g.
```
from c_conv import conv_torch
conv_torch.Conv.n_threads = 4  # Enables multithreading (disabled by default)

filter = torch.randn((10,3,28,52), requires_grad=True)
image = torch.randn((32,3,28,52), requires_grad=False)

res_c = conv_torch.cconv(filter, image, stride)

dummy_loss = torch.sum(res_c)
dummy_loss.backward()
filter.grad # now contains the gradient of the loss with respect to the filter
```

`conv_torch.Conv.n_threads=4` could be set to use 4 threads for the forward execution 
(default 1; parallelism for the backward pass is not yet implemented)

### From numpy
```
import conv

a = np.array([[1.,2.],[3.,4.]], dtype='float32')
image = np.eye(4,4, dtype='float32')

# Works on 4D arrays but broadcasts lower-dim ones
res = conv.conv(a,image,stride=2) 

# res= [[[[5. 0.]
#         [0. 5.]]]]

d_res = res
d_a, _ = conv.conv_bw(d_res, a, image, stride=2)
d_a # now contains the gradient with respect to the filter a (if d_res contained gradient w.r.t the output)

```

### Pure C
Note that the C code can of course be used also from other C scripts. The `src` directory also contains `conv_vec_test.c` to test that:
```bash
gcc conv_vec.c conv_vec_test.c -o ./ctest -Wall
./ctest
```
(Note that the compilation throws a bunch of warnings. This is because the library was originally written using multidimensional arrays. However, support for those with a variable number of dimensions has been added only recently to C and is not yet supported by Cython. The test works as is.)