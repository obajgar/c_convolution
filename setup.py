import setuptools
from Cython.Build import cythonize
import numpy as np

with open("README.md", "r") as fh:
    long_description = fh.read()

packages = setuptools.find_packages()
print(f"Setuptools found the following packages {packages}")

source_files = ["c_conv/src/conv.pyx", "c_conv/src/conv_vec.c", "c_conv/src/conv_paral.c"]

setuptools.setup(
    ext_modules = cythonize([setuptools.Extension("conv", source_files,
                                                  include_dirs=[np.get_include()])],
                            language_level="3"),

    name="c_conv",
    version="0.2.0",
    author="Ondrej Bajgar",
    author_email="ondrej@bajgar.org",
    description="2D convolution implemented in C",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/obajgar/c_convolution",
    packages=['c_conv'],
    install_requires=['Cython', 'numpy', 'torch', 'wheel', 'setuptools'],
    classifiers=[
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent"
    ],
)


